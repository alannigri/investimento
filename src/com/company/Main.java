package com.company;

public class Main {
    public static void main(String[] args) {
        IO io = new IO();
	    Cliente alan = new Cliente("Alan", 31, 10000);
	    Banco banco = new Banco();
	    Investimento investimento = new Investimento(io.getCapital(),io.getMeses());
        io.exibirMontante(alan.investir(investimento, banco));
    }
}