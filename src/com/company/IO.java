package com.company;

import java.util.Scanner;

public class IO {
    Scanner scanner = new Scanner(System.in);

    public void exibirMontante(double montante) {
        System.out.println("Seu investimento no fim terá o valor de R$" + montante);
    }

    public double getCapital() {
        System.out.println("Informe o capital para aplicar:");
        double capital = scanner.nextFloat();
        return capital;
    }

    public int getMeses() {
        System.out.println("Informe a quantidade de meses desejado:");
        int meses = scanner.nextInt();
        return meses;
    }

}

