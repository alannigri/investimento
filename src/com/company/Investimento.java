package com.company;

public class Investimento {
    private double capital;
    private int meses;

    public Investimento(double capital, int meses) {
        this.capital = capital;
        this.meses = meses;
    }

    public double investir(Banco banco){
        return banco.calculaRendimento(capital, meses);
    }
}
