package com.company;

public class Banco {
    private int conta;
    private double saldo, rendimento;
    private double taxa = 0.007;

        public double calculaRendimento(double capital, int quantidadeMeses){
        rendimento = 0;
        for(int i=0; i<quantidadeMeses;i++){
            if(i==0){
                rendimento = capital;
            }
            rendimento += rendimento*taxa;
        }
        return rendimento;
    }
}