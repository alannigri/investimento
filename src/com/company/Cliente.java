package com.company;

public class Cliente {
    private String nome;
    private int idade;
    private double salario;

    public Cliente(String nome, int idade, double salario) {
        this.nome = nome;
        this.idade = idade;
        this.salario = salario;
    }

    public double investir(Investimento investimento, Banco banco) {
        return investimento.investir(banco);
    }
}
